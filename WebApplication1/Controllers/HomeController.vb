﻿Imports System.Data.SqlClient

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Return View()
    End Function

    Function About() As ActionResult
        ViewData("Message") = "Your application description page."

        Return View()
    End Function

    Function Contact() As ActionResult
        ViewData("Message") = "Your contact page."

        Return View()
    End Function

    Function ListContainer(pEir As String)

        Dim strConn As String = "Initial Catalog=360Movil;Data Source=45.58.46.247;User Id=jrojas;Password=calama.;"

        Dim sqlCon As SqlConnection

        sqlCon = New SqlConnection(strConn)

        Dim eir = pEir

        Dim texto

        Using (sqlCon)

            Dim sqlComm As New SqlCommand()

            sqlComm.Connection = sqlCon

            sqlComm.CommandText = "ListContainer"
            sqlComm.CommandType = CommandType.StoredProcedure

            sqlComm.Parameters.AddWithValue("eir", eir)
            sqlCon.Open()

            'texto = Convert.ToString(sqlComm.ExecuteScalar())
            Dim dr = sqlComm.ExecuteReader




            While dr.Read
                

                ViewData("eir") = dr("eir")
                ViewData("contenedor") = dr("contenedor")
                ViewData("sello") = dr("sello")
                ViewData("responsable_nombre") = dr("responsable_nombre")
                ViewData("empresa") = dr("empresa_nombre")

            End While

            'ViewData("Message") = texto

        End Using




        Return View()

    End Function

End Class
